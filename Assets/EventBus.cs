﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Class;
using Events;
using Events.Handler;
using UnityEngine;
using Interface;

public class EventBus : MonoBehaviour
{
    IBoard _board;
    private GuiEvent _em;

    private void Start()
    {
        _em = new GuiEvent();
        _em.OnClickField += MoveHandler.OnClickMoveHandler;

        _board = GetComponentInChildren<IBoard>();
    }

    public void FieldSelected(Field field)
    {
        _em.ClickFieldMoveInvoke(field.positionX, field.positionZ);
    }

}
