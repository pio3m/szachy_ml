﻿using System.Collections.Generic;
using Class;
using UnityEngine;

namespace Interface
{
    public interface IBoard
    {
        void SpownBoard(int size);
        Field GetAreaLocated(int x, int z);
        void SelectFieldOnBoardToMove(List<CordMove> coordinates);
        void AllUnSelectField();
        void SelectMainField(CordMove move);
        PieceColor GetPieceColorOnField(CordMove cords);
        bool CheckIfFieldIsActive(Field mainField);
        Field GetField(CordMove mainSelect);
        bool CheckIfMainField(Field field);
        CordMove GetMainFieldCords();
        void ChangeMainField(Field clickedField, IFigure figure);
        void SelectFieldOnBoardToTake(List<CordMove> canTake);
    }
}