﻿using System;
using System.Collections.Generic;
using Class;
using UnityEngine;

namespace Interface
{
    public interface IFigure
    {
        List<CordMove> GetFieldCanMove(CordMove cordMove);
        PieceColor GetPieceColor();
        void SetColorPiece(PieceColor color);
        List<CordMove> GetFieldCanTake(CordMove cordMove);
        //UI Canvas
        void ViewCanvasTransformation();
        void HideCanvas();
        void SetWorldCamera(Camera worldCamera);
        FigureType GetFigureType();
        Field GetField();
        void SetField(Field field);
    }

    public enum FigureType
    {
        Pawn,
        Bishop,
        Hors,
        Rock,
        King,
        Qeen
    }
}