﻿using Class;
using UnityEngine;

namespace Interface
{
   public interface IPiece
   {
      GameObject SpownPown(PieceColor color, Field field);

      GameObject Spown(PieceType typeFigure, PieceColor color, Field getAreaLocated);
   }
}
