﻿using System.Collections.Generic;
using System.Linq;

namespace Class
{
    // public class Move
    // {
    //     private  int
    //
    //     public Move(int xPos = -1, int zPos = -1)
    //     {
    //         if (xPos != -1 || zPos != -1)
    //         {
    //             _baseCord = new BaseMove(xPos, zPos);
    //         }
    //     }
    //
    //     public void AddCordMove(int x, int z)
    //     {
    //         //spr czy na area jest jakaś figura - jeśli tak to nie dodawaj
    //         _moveCord.Add(x, z);
    //     }
    //
    //     public Dictionary<int, int> GetMove()
    //     {
    //         return _moveCord;
    //     }
    //
    //     public BaseMove GetBaseMove()
    //     {
    //         return _baseCord;
    //     }
    // }
    public class Move
    {
       public int Col;
       public int Row;     

       public Move(int col, int row)
       {
           Col = col;
           Row = row;
       }

    }

    public struct CordMove
    {
        public int Column;
        public int Rows;

        public CordMove(int col, int row)
        {
            Column = col;
            Rows = row;
        }
    }
}