﻿using System;
using Class;
using Interface;
using UnityEngine;

public class TransformationFigure : MonoBehaviour
{
    private PieceManager _pieceManager;

    private void Start()
    {
        _pieceManager = FindObjectOfType<PieceManager>();
    }

    /// <summary>
    /// Call from canvas
    /// </summary>
    /// <param name="gm"></param>
    public void TransformateFigure(GameObject gm)
    {
        // stara gm zamieniasz na nowy
        var figure = gm.GetComponentInChildren<IFigure>();
        var colorToTransform = figure.GetPieceColor();
        figure.HideCanvas();
        Destroy(gm);
        _pieceManager.SpownQueen(colorToTransform, figure.GetField());

        //wkaldasz na fielda
    }
}