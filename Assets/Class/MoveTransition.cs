﻿using UnityEngine;

namespace Class
{
    public class MoveTransition : MonoBehaviour
    {
        bool _isMoving = false;
        float speed = 1;
        private Vector3 _toPos;
        private GameObject _figureToMove;

        void Update()
        {
            if (_isMoving)
            {
                var actualPos = _figureToMove.transform.position;
                _figureToMove.transform.position = Vector3.Lerp(actualPos, _toPos, Time.deltaTime * speed);
                
                if (Vector3.Distance(_figureToMove.transform.position, _toPos) < 0.02 && _isMoving)
                    _isMoving = false;
            }

            
        }

        public void MakeMove(Vector3 pos, GameObject figureToMove)
        {
            var pozY = figureToMove.GetComponent<Transform>().position.y;
            pos.y = pozY;
            _toPos = pos;
            _isMoving = true;
            _figureToMove = figureToMove;
        }
    }
}