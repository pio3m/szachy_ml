﻿using System;
using System.Collections.Generic;
using System.Linq;
using Class;
using Interface;
using UnityEngine;

public class Figures : MonoBehaviour, IFigureCords
{
    private Dictionary<CordMove, GameObject> figures = new Dictionary<CordMove, GameObject>();
    public GameObject GetFigureOnCords(CordMove cords)
    {
        foreach (KeyValuePair<CordMove, GameObject> move in figures)
        {
            if (move.Key.Column == cords.Column &&
                move.Key.Rows == cords.Rows)
            {
                return move.Value;
            }
        }

        throw new Exception("No Game Object on field");
    }

    public void ChangeFigureCords(CordMove oldCords, CordMove newCords)
    {
        var moveRem =
            figures.Keys.First(x => x.Column == oldCords.Column && x.Rows == oldCords.Rows);
        var obj =
            figures.First(x => x.Key.Column == oldCords.Column && x.Key.Rows == oldCords.Rows);
        var field = 
     
        figures.Remove(moveRem);
        figures.Add(newCords, obj.Value);
    }

    public void RemoveFigureFromBoard(CordMove clickedMove)
    {
        var moveRem =
            figures.Keys.First(x => x.Column == clickedMove.Column && x.Rows == clickedMove.Rows);
        figures.Remove(moveRem);
    }

    public void FigureTransformation(GameObject figureToMove, Field clickedField)
    {
        throw new NotImplementedException();
    }

    public void AddFigure(CordMove move, GameObject figure)
    {
        figures.Add(move, figure);
    }
}

public interface IFigureCords
{
    void AddFigure(CordMove move, GameObject figure);
    GameObject GetFigureOnCords(CordMove move);
    void ChangeFigureCords(CordMove oldMove, CordMove newMove);
    void RemoveFigureFromBoard(CordMove clickedMove);
    void FigureTransformation(GameObject figureToMove, Field clickedField);
}