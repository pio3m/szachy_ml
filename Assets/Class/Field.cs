﻿using Interface;
using UnityEngine;

namespace Class
{
    public class Field : MonoBehaviour
    {
        [SerializeField] private Material selectedMaterial;
        [SerializeField] private Material moveMaterial;
        [SerializeField] private Material takeMaterial;
        private bool _selectedForMove;

        private Material _mainMaterial;

        // from 1-8
        public int positionX;

        //from A-H
        public int positionZ;

        //aktualna figura na polu
        private IFigure _figure;

        private void Start()
        {
            _mainMaterial = GetComponentInChildren<Renderer>().material;
        }

        public void SelectField()
        {
            GetComponentInChildren<Renderer>().material = selectedMaterial;
        }

        public void SetFigureOnField(IFigure figure)
        {
            _figure = figure;
            _figure.SetField(this);
        }

        public IFigure GetFigure()
        {
            return _figure;
        }

        public void UnSelected()
        {
            this.GetComponentInChildren<Renderer>().material = _mainMaterial;
        }

        public void SelectEmptyField()
        {
            GetComponentInChildren<Renderer>().material = moveMaterial;
            _selectedForMove = true;
        }

        public void SelectCanTakeField()
        {
            GetComponentInChildren<Renderer>().material = takeMaterial;
            _selectedForMove = true;
        }

        public PieceColor GetFigureColor()
        {
            return _figure.GetPieceColor();
        }

        public void MoveOutFigure()
        {
            _figure = null;
        }

        public bool HaveFigure()
        {
            return _figure != null;
        }

        public void TakeFigureFromField()
        {
            _figure = null;
        }
    }
}