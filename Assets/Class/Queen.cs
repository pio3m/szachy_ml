﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Class;
using Interface;
using UnityEngine;

public class Queen : MonoBehaviour, IFigure
{
    private bool _bind = false;
    public PieceColor _color;
    public GameObject _transformationCanvas;
    public TowerMove _towerMove;

    private BoardManager _board;
    private Field _actualField;

    // Start is called before the first frame update
    void Start()
    {
        _board = GetComponentInParent<BoardManager>();
    }

    public List<CordMove> GetFieldCanMove(CordMove cordMove)
    {
        List<CordMove> moves = new List<CordMove>();
        var xPos = cordMove.Column;
        var zPos = cordMove.Rows;
        var actualCord = new CordMove(_actualField.positionX, _actualField.positionZ);

        moves = (_towerMove.GetMove(actualCord));

        return moves;
    }

    public PieceColor GetPieceColor()
    {
        return _color;
    }

    public void SetColorPiece(PieceColor color)
    {
        _color = color;
    }

    public List<CordMove> GetFieldCanTake(CordMove cordMove)
    {
        List<CordMove> moves = new List<CordMove>();

        return moves;
    }

    public void ViewCanvasTransformation()
    {
        throw new System.NotImplementedException();
    }

    public void HideCanvas()
    {
        throw new System.NotImplementedException();
    }

    public FigureType GetFigureType()
    {
        return FigureType.Qeen;
    }

    public void SetWorldCamera(Camera worldCamera)
    {
        throw new System.NotImplementedException();
    }

    public Field GetField()
    {
        throw new System.NotImplementedException();
    }

    public void SetField(Field field)
    {
        _actualField = field;
    }
}