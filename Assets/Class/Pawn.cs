﻿using System.Collections.Generic;
using Interface;
using UnityEngine;

namespace Class
{
    public class Pawn : MonoBehaviour, IFigure
    {
        //związanie figury (nie może się ruszyć bo odsłoni króla) 
        private bool _bind = false;
        public PieceColor _color;
        public GameObject _transformationCanvas;
        private BoardManager _board;
        private Field _actualField;

        private void Start()
        {
            _board = GetComponentInParent<BoardManager>();
        }
        public PieceColor GetPieceColor()
        {
            return _color;
        }

        public void SetColorPiece(PieceColor color)
        {
            _color = color;
            
        }

        public List<CordMove> GetFieldCanMove(CordMove cordMove)
        {
            List<CordMove> moves = new List<CordMove>();

            var xPos = cordMove.Column;
            var zPos = cordMove.Rows;

            AddMoveOneField(moves, xPos, zPos);
            AddMoveTwoField(moves, xPos, zPos);

            return moves;
        }

        private void AddMoveTwoField(List<CordMove> moves, int xPos, int zPos)
        {
            if (_color == PieceColor.White && zPos == 1 ||
                _color == PieceColor.Black && zPos == 6)
            {
                if (_color == PieceColor.White)
                {
                    if (!CheckCanMove(xPos, zPos + 2)) return;
                    var move = new CordMove(xPos, zPos + 2);
                    moves.Add(move);
                }
                else
                {
                    if (!CheckCanMove(xPos, zPos - 2)) return;
                    var move = new CordMove(xPos, zPos - 2);
                    moves.Add(move);
                }
            }
        }

        private void AddMoveOneField(List<CordMove> moves, int xPos, int zPos)
        {
            if (CheckCanMove(xPos, zPos + 1) & _color == PieceColor.White)
            {
                var moveChangedW = OneFieldPownMove(xPos, zPos);
                moves.Add(moveChangedW);
            }

            if (CheckCanMove(xPos, zPos - 1) & _color == PieceColor.Black)
            {
                var moveChangedB = OneFieldPownMove(xPos, zPos);
                moves.Add(moveChangedB);
            }
        }

        private CordMove OneFieldPownMove(int xPos, int zPos)
        {
            if (_color == PieceColor.White)
            {
                var move = new CordMove(xPos, zPos + 1);
                return move;
            }
            else
            {
                var move = new CordMove(xPos, zPos - 1);
                return move;
            }
        }

        private bool CheckCanMove(int xPos, int zPos)
        {
            var area = _board.GetAreaLocated(xPos, zPos);
            var isFigureOnField = area.HaveFigure();
            return !isFigureOnField;
        }

        public List<CordMove> GetFieldCanTake(CordMove cordMove)
        {
            //dodać bicie w przelocie

            var canTake = new List<CordMove>();

            var moveTake = OneFieldPownMove(cordMove.Column, cordMove.Rows);

            AddMoveTake(canTake, moveTake);


            return canTake;
        }

        public void ViewCanvasTransformation()
        {
            _transformationCanvas.SetActive(true);
        }

        public void HideCanvas()
        {
            _transformationCanvas.SetActive(false);
        }

        public FigureType GetFigureType()
        {
            return FigureType.Pawn;
        }

        public void SetWorldCamera(Camera worldCamera)
        {
            _transformationCanvas.GetComponent<Canvas>().worldCamera = worldCamera;
        }

        public Field GetField()
        {
            return _actualField;
        }

        public void SetField(Field field)
        {
            _actualField = field;
        }

        private void AddMoveTake(List<CordMove> canTake, CordMove move)
        {
            if (move.Column - 1 >= 0)
            {
                canTake.Add(new CordMove(move.Column - 1, move.Rows));
            }

            if (move.Column + 1 < 8)
            {
                canTake.Add(new CordMove(move.Column + 1, move.Rows));
            }
        }
    }
}