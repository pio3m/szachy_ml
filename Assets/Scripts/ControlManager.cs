﻿using Class;
using Events;
using Events.Handler;
using UnityEngine;
using System.Collections.Generic;
using Interface;
public class ControlManager : MonoBehaviour
{
    public Camera camera;
    IBoard _board;
    public EventBus bus;
    private Field selectedField;
    private GameManager _gm;
    private GuiEvent _em;


    private void Start()
    {
        _em = new GuiEvent();
        _em.OnClickField += MoveHandler.OnClickFieldHandler;
        _gm = GetComponentInParent<GameManager>();
        _board = GetComponentInChildren<IBoard>();
    }



    // Update is called once per frame
    private void Update()
    {
        if (!Input.GetMouseButtonDown(0)) return;

        var mousePoint = Input.mousePosition;

        var ray = camera.ScreenPointToRay(mousePoint);

        if (!Physics.Raycast(ray, out var hit)) return;
        selectedField = hit.transform.GetComponent<Field>();

        bus.FieldSelected(selectedField);

        //send reqest for field to select

        // var fieldToSelect = new List<CordMove>();
        // var move1 = new CordMove(0,1);
        // fieldToSelect.Add(move1);

        // //send reqest to GUI 
       // _board.SelectFieldOnBoardToMove(fieldToSelect);

        //_em.ClickField(selectedField.positionX, selectedField.positionZ);


        _gm.ClickField(selectedField.positionX, selectedField.positionZ);
    }
}