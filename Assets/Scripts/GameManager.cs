﻿using System.Collections.Generic;
using System.Linq;
using Class;
using Interface;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    IBoard _board;
    IPiece _piece;
    private IFigureCords _figureCords;

    private List<CordMove> moveHistory;


    PieceColor actualColorToMove;

    private void Start()
    {
        _board = GetComponentInChildren<IBoard>();
        _piece = GetComponentInChildren<IPiece>();
        _figureCords = GetComponentInChildren<IFigureCords>();

        _board.SpownBoard(8);
        CreateWhitePowns();
        CreateBlackPowns();

        SpownQueen(PieceColor.White, new CordMove(4, 0));
        SpownQueen(PieceColor.Black, new CordMove(4, 7));


        actualColorToMove = PieceColor.White;
    }


    private void SpownQueen(PieceColor color, CordMove move)
    {
        _figureCords.AddFigure(move,
            _piece.Spown(PieceType.Queen, color,
                _board.GetAreaLocated(move.Column, move.Rows)));
    }


    private void CreateWhitePowns()
    {
        List<CordMove> movesToSpown = new List<CordMove>();
        movesToSpown.Add(new CordMove(0, 1));
        movesToSpown.Add(new CordMove(1, 1));
        movesToSpown.Add(new CordMove(2, 1));
        movesToSpown.Add(new CordMove(3, 1));
        movesToSpown.Add(new CordMove(4, 1));
        movesToSpown.Add(new CordMove(5, 1));
        movesToSpown.Add(new CordMove(6, 1));
        movesToSpown.Add(new CordMove(7, 1));

        foreach (var move in movesToSpown)
        {
            SpownPown(move, PieceColor.White);
        }
    }

    private void CreateBlackPowns()
    {
        List<CordMove> movesToSpown = new List<CordMove>();
        movesToSpown.Add(new CordMove(0, 6));
        movesToSpown.Add(new CordMove(1, 6));
        movesToSpown.Add(new CordMove(2, 6));
        movesToSpown.Add(new CordMove(3, 6));
        movesToSpown.Add(new CordMove(4, 6));
        movesToSpown.Add(new CordMove(5, 6));
        movesToSpown.Add(new CordMove(6, 6));
        movesToSpown.Add(new CordMove(7, 6));

        foreach (var move in movesToSpown)
        {
            SpownPown(move, PieceColor.Black);
        }
    }

    private void SpownPown(CordMove move, PieceColor color)
    {
        _figureCords.AddFigure(move,
            _piece.SpownPown(color,
                _board.GetAreaLocated(move.Column, move.Rows)));
    }

    public void ClickField(int selectedFieldPositionX, int selectedFieldPositionZ)
    {
        //check what color moving
        var clickedMove = new CordMove(selectedFieldPositionX, selectedFieldPositionZ);
        var clickedField = _board.GetField(clickedMove);


        if (!_board.CheckIfFieldIsActive(clickedField))
        {
            if (!clickedField.HaveFigure())
                return;

            var pieceColor = _board.GetPieceColorOnField(clickedMove);

            if (pieceColor != actualColorToMove)
                return;

            _board.AllUnSelectField();
            _board.SelectMainField(clickedMove);

            var clickedFigure = clickedField.GetFigure();
            var canMove = clickedFigure.GetFieldCanMove(clickedMove);
            _board.SelectFieldOnBoardToMove(canMove);

            var canPotentiallyTake = clickedFigure.GetFieldCanTake(clickedMove);
            var canTake = CheckFieldCanTakeHaveFigure(canPotentiallyTake);


            if (canTake.Any())
                _board.SelectFieldOnBoardToTake(canTake);

            return;
        }

        if (_board.CheckIfMainField(clickedField)) return;

        // var posToMove = _board.GetAreaLocated(clickedMove.GetBaseMove().xPos, clickedMove.GetBaseMove().zPos);


        if (clickedField.HaveFigure())
        {
            clickedField.TakeFigureFromField();
            var figToTake = _figureCords.GetFigureOnCords(new CordMove(clickedField.positionX, clickedField.positionZ));
            figToTake.SetActive(false);
            _figureCords.RemoveFigureFromBoard(clickedMove);
        }

        var figureToMove = _figureCords.GetFigureOnCords(_board.GetMainFieldCords());
        figureToMove.GetComponentInChildren<MoveTransition>()
            .MakeMove(clickedField.gameObject.transform.position, figureToMove);

        _figureCords.ChangeFigureCords(_board.GetMainFieldCords(), clickedMove);

        //change figure to new field
        _board.ChangeMainField(clickedField, figureToMove.GetComponentInChildren<IFigure>());

        //check if figure is pown
        var figureType = figureToMove.GetComponentInChildren<IFigure>().GetFigureType();
        if (figureType == FigureType.Pawn)
        {
            if (figureToMove.GetComponentInChildren<IFigure>().GetPieceColor() == PieceColor.White)
            {
                if (clickedMove.Rows == 7)
                    figureToMove.GetComponentInChildren<IFigure>().ViewCanvasTransformation();
            }
            else
            {
                if (clickedMove.Rows == 0)
                    figureToMove.GetComponentInChildren<IFigure>().ViewCanvasTransformation();
            }
        }
        //if pown is on first line [0]

        //transform

        //_figureCords.FigureTransformation(figureToMove, clickedField);

        _board.AllUnSelectField();
        ToggleColor(actualColorToMove);
    }

    private List<CordMove> CheckFieldCanTakeHaveFigure(List<CordMove> canPotentiallyTake)
    {
        var moveTake = new List<CordMove>();
        foreach (var move in canPotentiallyTake)
        {
            var field = _board.GetAreaLocated(move.Column, move.Rows);
            if (field.HaveFigure() && field.GetFigureColor() != actualColorToMove)
            {
                moveTake.Add(move);
            }
        }

        return moveTake;
    }

    private void ToggleColor(PieceColor pieceColor)
    {
        if (pieceColor == PieceColor.White)
        {
            actualColorToMove = PieceColor.Black;
        }
        else
        {
            actualColorToMove = PieceColor.White;
        }
    }
}

public enum PieceColor
{
    Black,
    White
}

public enum PieceType
{
    Knight,
    Queen,
    Bishop,
    Rock,
    Pown,
    King
}