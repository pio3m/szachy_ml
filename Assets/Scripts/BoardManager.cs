﻿using System;
using System.Collections.Generic;
using System.Linq;
using Class;
using Interface;
using UnityEngine;

public class BoardManager : MonoBehaviour, IBoard
{
    [SerializeField] private GameObject WhiteArea;
    [SerializeField] private GameObject BlackArea;

    //Gameobject with IFields
    private List<Field> _areas = new List<Field>();
    private List<Field> _canMove = new List<Field>();
    private List<Field> _selectedFields = new List<Field>();
    private Field _mainField;

    public void SpownBoard(int size)
    {
        for (int x = 0; x < size; x++)
        {
            for (int z = 0; z < size; z++)
            {
                if (x % 2 == 0)
                {
                    GameObject area = z % 2 == 0 ? WhiteArea : BlackArea;
                    CreateArea(x, z, 0, area);
                }
                else
                {
                    GameObject area = z % 2 == 0 ? BlackArea : WhiteArea;
                    CreateArea(x, z, 0, area);
                }
            }
        }
    }

    public bool CheckIfMainField(Field field)
    {
        return field.positionX == _mainField.positionX && field.positionZ == _mainField.positionZ;
    }


    public CordMove GetMainFieldCords()
    {
        return new CordMove(_mainField.positionX, _mainField.positionZ);
    }

    public void ChangeMainField(Field clickedField, IFigure figure)
    {
        _mainField.MoveOutFigure();
        _mainField = clickedField;
        clickedField.SetFigureOnField(figure);
    }

    public void SelectFieldOnBoardToTake(List<CordMove> canTake)
    {
        List<Field> goFields = new List<Field>();
        foreach (var coordinate in canTake)
        {
            var move = coordinate;
            goFields.Add(FindFieldWithCords(move.Column, move.Rows));
        }

        if (!goFields.Any())
            return;

        foreach (var field in goFields)
        {
            _canMove.Add(field);
            _selectedFields.Add(field);
            field.SelectCanTakeField();
        }
    }

    public Field GetAreaLocated(int x, int z)
    {
        foreach (var goField in _areas)
        {
            var f = goField.GetComponent<Field>();
            if (f.positionX == x && f.positionZ == z)
            {
                return goField;
            }
        }

        throw new Exception("Field not found");
    }

    public void SelectFieldOnBoardToMove(List<CordMove> coordinates)
    {
        List<Field> goFields = new List<Field>();
        foreach (var coordinate in coordinates)
        {
            goFields.Add(FindFieldWithCords(coordinate.Column, coordinate.Rows));
        }

        if (!goFields.Any())
            return;

        foreach (var field in goFields)
        {
            _canMove.Add(field);
            _selectedFields.Add(field);
            field.SelectEmptyField();
        }
    }

    public void AllUnSelectField()
    {
        foreach (var field in _selectedFields)
        {
            UnSelectField(field);
        }

        _selectedFields.Clear();
    }


    public void UnSelectField(Field field)
    {
        field.UnSelected();
    }

    public void SelectMainField(CordMove move)
    {
        var field = GetField(move);
        field.SelectField();
        _mainField = field;
        _selectedFields.Add(field);
    }

    public PieceColor GetPieceColorOnField(CordMove move)
    {
        var baseMove = move;
        var field = FindFieldWithCords(baseMove.Column, baseMove.Rows);
        var clo = field.GetFigureColor();
        return clo;
    }

    public bool CheckIfFieldIsActive(Field mainField)
    {
        return _selectedFields.Contains(mainField);
    }

    public Field GetField(CordMove move)
    {
        var baseMove = move;
        return FindFieldWithCords(baseMove.Column, baseMove.Rows);
    }

    private Field FindFieldWithCords(int coordinateKey, int coordinateValue)
    {
        foreach (Field field in _areas)
        {
            if (field.positionX == coordinateKey && field.positionZ == coordinateValue)
                return field;
        }

        return null;
    }

    private void CreateArea(int x, int z, int y, GameObject area)
    {
        var chessArea = Instantiate(area, new Vector3(x * 10f, y, z * 10f), Quaternion.identity, this.transform);
        var cField = chessArea.GetComponent<Field>();
        cField.positionX = x;
        cField.positionZ = z;
        _areas.Add(cField);
    }
}