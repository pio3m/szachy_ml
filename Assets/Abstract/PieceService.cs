﻿
using UnityEngine;

namespace Abstract
{
    public abstract class PieceService 
    {
        private PieceColor _color;

        public PieceColor GetPieceColor()
        {
            return _color;
        }

        public void SetColorPiece(PieceColor color)
        {
            _color = color;
            
        }
    }
}