﻿using System.Collections.Generic;
using Class;
using UnityEngine;

public class TowerMove : MonoBehaviour
{
    List<CordMove> potentialyMoves = new List<CordMove>();
    private BoardManager _board;

    int maxVertical = 7;
    int minVertical = 0;

    private int maxHorizontal = 7;
    private int minHorizontal = 0;

    private void Start()
    {
        _board = GetComponentInParent<BoardManager>();
    }

    public List<CordMove> GetMove(CordMove cord)
    {
        var returnList = new List<CordMove>();
        
        //dodaj wszystkie pola w górę
        for (var i = cord.Rows + 1; i <= maxVertical; i++)
        {
            returnList.Add(new CordMove(cord.Column, i));
        }

        //dodaj wszsytkie pola w dół
        for (var j = cord.Rows - 1; j >= minVertical; j--)
        {
            returnList.Add(new CordMove(cord.Column, j));
        }

        //dodaj wszsytkie pola w lewo
        for (var k = cord.Column + 1; k <= maxHorizontal; k++)
        {
            returnList.Add(new CordMove(k, cord.Rows));
        }

        //dodaj wszystkie pola w prawo
        for (var l = cord.Column - 1; l >= minHorizontal; l--)
        {
            returnList.Add(new CordMove(l, cord.Rows));
        }

        return returnList;
    }
}