﻿using System;

namespace Events
{
    public class GuiEvent : IGuiEvents
    {
        public event EventHandler<MoveArgs> OnClickField;
        public event EventHandler OnUndoLastMove;
        public event EventHandler OnStartClock;
        public event EventHandler OnMovePieceOnField;

        public void ClickFieldMoveInvoke(int x, int y)
        {
            OnClickField?.Invoke(this, new MoveArgs(x, y));
        }

        public void UndoLastMoveInvoke()
        {
            OnUndoLastMove?.Invoke(this, EventArgs.Empty);
        }

        public void StartClock(PieceColor color)
        {
            OnStartClock?.Invoke(this, new PieceColorArgs(color));
        }

        public void MovePieceOnField(int x, int y)
        {
            OnMovePieceOnField?.Invoke(this, new MoveArgs(x, y));
        }
    }

    public interface IGuiEvents
    {
        void ClickFieldMoveInvoke(int x, int y);
        void UndoLastMoveInvoke();
        void StartClock(PieceColor color);
        void MovePieceOnField(int x, int y);
    }

    public class MoveArgs : EventArgs
    {
        public MoveArgs(int a, int b)
        {
            x = a;
            y = b;
        }

        public int x;
        public int y;
    }

    public class PieceColorArgs : EventArgs
    {
        private PieceColor _color;

        public PieceColorArgs(PieceColor color)
        {
            _color = color;
        }
    }
}