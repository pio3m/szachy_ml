﻿using System;
using UnityEngine;

namespace Events.Handler
{
    public class MoveHandler
    {
        public static void OnClickMoveHandler(object sender, MoveArgs e)
        {
            //var move = e as MoveArgs;
            var x = e.x;
            var y = e.y;
           Debug.Log("Generate move for " + x + " " + y );
        }

        public static void OnClickFieldHandler(object sender, EventArgs e)
        {
            Debug.Log("Generate move " );
        }
    }
}    