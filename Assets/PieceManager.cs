﻿using System;
using System.Collections;
using Class;
using Interface;
using UnityEngine;

public class PieceManager : MonoBehaviour, IPiece
{
    [SerializeField] private GameObject WhitePown;
    [SerializeField] private GameObject BlackPown;

    [SerializeField] private GameObject Queen;

    [SerializeField] private GameObject Tower;

    [SerializeField] private GameObject Knight;

    [SerializeField] private GameObject Bishop;

    [SerializeField] private Material blackMaterial;
    [SerializeField] private Material whiteMaterial;
    public Camera worldCamera;

    public GameObject SpownPown(PieceColor color, Field field)
    {
        GameObject pown = null;

        if (color == PieceColor.White)
        {
            pown = WhitePown;
        }
        else
        {
            pown = BlackPown;
        }

        pown.GetComponent<IFigure>().SetColorPiece(color);
        
        
        var transform1 = field.transform;
        var position = transform1.position;
        var gm = Instantiate(pown,
            new Vector3(position.x, position.y + 1f, position.z),
            Quaternion.Euler(-90, 0, 0), field.transform);

        field.SetFigureOnField(gm.GetComponent<IFigure>());
        gm.GetComponent<IFigure>().SetWorldCamera(worldCamera);
        
        return gm;
    }

    public GameObject Spown(PieceType typeFigure, PieceColor color, Field field)
    {
        GameObject objToSpown = GetFigure(typeFigure);

        if (color == PieceColor.Black)
        {
            objToSpown.GetComponent<Renderer>().material = blackMaterial;
        }
        else
        {
            objToSpown.GetComponent<Renderer>().material = whiteMaterial;
        }

        objToSpown.GetComponent<IFigure>().SetColorPiece(color);

        var yOffset = 10f;
        if (typeFigure == PieceType.Queen)
            yOffset = 9f;
        var transform1 = field.transform;
        var position = transform1.position;
        var gm = Instantiate(objToSpown,
            new Vector3(position.x, position.y + yOffset, position.z),
            Quaternion.Euler(-90, 0, 0), field.transform);

        field.SetFigureOnField(gm.GetComponent<IFigure>());

        return gm;
    }

    private GameObject GetFigure(PieceType typeFigure)
    {
        switch (typeFigure)
        {
            case PieceType.Knight:
                break;
            case PieceType.Bishop:
                break;
            case PieceType.Rock:
                break;
            case PieceType.Pown:
                break;
            case PieceType.King:
                break;
        }

        return Queen;
    }

    public void SpownQueen(PieceColor color, Field field)
    {
        Spown(PieceType.Queen, color, field);
    }
}